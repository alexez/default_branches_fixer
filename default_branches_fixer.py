#!/usr/bin/env python3
import re
import sys
from typing import NoReturn
import time

import gitlab
import urllib3

gitlab_url = 'https://server.url/'
branches_file_name = 'branches.list'
sleep_time = 1


def auth(secret_token: str) -> gitlab.Gitlab:
    urllib3.disable_warnings()
    gitlab_api = gitlab.Gitlab(gitlab_url, private_token=secret_token, ssl_verify=False)
    gitlab_api.auth()

    return gitlab_api


def main(secret_token, is_dumping: bool):
    api = auth(secret_token)

    if is_dumping:
        projects = api.projects.list(all=True)
        with open(branches_file_name, 'w') as branches_file:
            for project in projects:
                print('default branch for project "{}" (with ID = {}) is "{}"'.format(
                    project.path_with_namespace,
                    project.id,
                    project.default_branch)
                )
                branches_file.write('{} "{}" "{}"\n'.format(
                    project.id,
                    project.path_with_namespace,
                    project.default_branch)
                )
    else:
        regex = re.compile(r'(\d+) \"(.+)\" \"(.+)\"')
        with open(branches_file_name, 'r') as branches_file:
            for line in branches_file.readlines():
                result = regex.match(line)
                project_id: int = int(result.group(1))
                proper_default_branch: str = result.group(3)
                project = api.projects.get(project_id)
                is_archived: bool = project.archived

                print(
                    'project "{}" (with ID = {}), archived: {}'
                    .format(
                        result.group(2),
                        result.group(1),
                        is_archived,
                    )
                )

                branches = project.branches.list()
                if len(branches) == 0:
                    print(
                        '\tskipped since there are no branches'
                        .format(
                            result.group(2),
                            result.group(1)
                        )
                    )
                    continue

                default_branch_before: str = project.default_branch

                if is_archived:
                    project.unarchive()
                    print('\tunarchived')

                if default_branch_before == proper_default_branch:
                    print('\tdefault is properly set to "{}" but we don\'t care'.format(proper_default_branch))
                    if len(branches) == 1:
                        base_branch_name: str = branches[0].name
                        print('\tthere is only one branch', base_branch_name)

                        temporary_branch = project.branches.create(
                            {'branch': 'unique_name_for_temporary_branch', 'ref': base_branch_name})
                        print('\ttemporary branch "{}" created'.format(temporary_branch.name))

                        project.default_branch = temporary_branch.name
                        project.save()
                        print('\tdefault branch switched to', temporary_branch.name, project.default_branch)

                        while True:
                            project.default_branch = proper_default_branch
                            project.save()
                            time.sleep(sleep_time)
                            project.refresh()
                            if project.default_branch == proper_default_branch:
                                break
                            else:
                                print(
                                    '\tretrying to set default branch (current: "{}", should be: "{}")'
                                    .format(
                                        project.default_branch,
                                        proper_default_branch
                                    ))
                        print('\tdefault branch switched back to', proper_default_branch, project.default_branch)

                        temporary_branch.delete()
                        print('\ttemporary branch removed')
                    else:
                        non_default_branch_name: str = \
                            [branch for branch in branches if branch.name != default_branch_before][0].name
                        print('\tthere are several branches, the chosen one is', non_default_branch_name)

                        project.default_branch = non_default_branch_name
                        project.save()
                        print('\tdefault branch switched to', non_default_branch_name, project.default_branch)

                        while True:
                            project.default_branch = proper_default_branch
                            project.save()
                            time.sleep(sleep_time)
                            project.refresh()
                            if project.default_branch == proper_default_branch:
                                break
                            else:
                                print(
                                    '\tretrying to set default branch (current: "{}", should be: "{}")'
                                    .format(
                                        project.default_branch,
                                        proper_default_branch
                                    ))
                        print('\tdefault branch switched to', proper_default_branch, project.default_branch)
                else:
                    print('\tdefault is not properly set (current: "{}", should be: "{}")'.format(
                        default_branch_before,
                        proper_default_branch
                    ))
                    project.default_branch = proper_default_branch
                    project.save()
                    print('\tdefault branch switched to', proper_default_branch, project.default_branch)

                if is_archived:
                    project.archive()
                    print('\tarchived back')


def exit_with_usage() -> NoReturn:
    print("Usage:\n\t{} your_secret_token (dump|apply)".format(sys.argv[0]))
    exit(1)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        exit_with_usage()
    if sys.argv[2] == 'dump':
        main(sys.argv[1], True)
    elif sys.argv[2] == 'apply':
        main(sys.argv[1], False)
    else:
        exit_with_usage()
