### Fixer for default branch

#### Installation

pip for Python version 3 should be installed

1 and 2 items are optional, do them if you don't want to litter your system with Python's packages.

1 (optional). Create virtual environment:
```bash
sudo apt install python3-venv
python3 -m venv venv
```

2 (optional). Activate virtual environment:
```bash
source venv/bin/activate
```

3 (required). Install requirements:
```bash
python3 -m pip install -r requirements.txt
```

#### Usage

```bash
./default_branches_fixer.py <your_secret_token> dump # to get proper default branches
./default_branches_fixer.py <your_secret_token> apply # to apply proper default branches
```
Secret token should be retrieved in GitLab settings
